resource "google_compute_instance" "vm_instances" {
  count        = length(var.vm_name)
  name         = var.vm_name[count.index]
  machine_type = var.vm_type[count.index]
  zone         = var.zone[count.index]
  
  tags = ["devops", "pipelines"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      labels = {
        my_label = "devops"
      }
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral public IP
    }
  }
}