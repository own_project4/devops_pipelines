variable "project_id" {
  type = string
}

variable "vm_name" {
  type = list(string)
}

variable "vm_type" {
  type = list(string)
}

variable "zone" {
  type = list(string)
}

variable "region" {
  type = string
}