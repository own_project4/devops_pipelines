terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.31.1"
    }
  }
}

provider "google" {
  project = var.project_id
  region = var.region
  credentials = "creds.json"
}


resource "google_project_service" "api" {
  for_each = toset([
    "compute.googleapis.com",
    "storage.googleapis.com",
    "cloudresourcemanager.googleapis.com",

  ])

  project = var.project_id
  service = each.key
}